//
//  Type.h
//  Autotester
//
//  Created by Joshua Trate on 5/8/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#ifndef __Autotester__Type__
#define __Autotester__Type__
#include <string>

using namespace std;


enum type
{
    INT,
    STRING,
    DOUBLE,
    BOOLEAN,
    CHAR,
    FLOAT,
    VOID,
    CUSTOM
};

/*!
 *  takes a type name such as int or double and returns
 *  a custom data type that stores it
 *
 *  @param type_in a string holding a type name such as "int"
 *
 *  @return the coresponding type of variable, from the type enum.
 */

type getType(string type_in);

/*!
 *  returns a string representing the apropriate type for declaration;
 *
 *  @param type_in a varaibe of type type that you want the code representation of
 *
 *  @return a string holding the code that represents a type
 */
string getTypeAsString(type type_in);


#endif /* defined(__Autotester__Type__) */
