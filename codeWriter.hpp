//
//  codeWriter.h
//  Autotester
//
//  Created by Joshua Trate on 5/4/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#ifndef __Autotester__codeWriter__
#define __Autotester__codeWriter__

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cassert>

using namespace std;

/*!
 * @class Codefile
 * a class that defines a single c++ file that can be written to disk
 * @abstract This class could be useful for writing single lines to a file in a syntactically correct way
 * this class handles indentation. all commands handle the space after them, ie start block adds a cury brace
 * and increments numTabs, then it adds a new line and indentation for the start of the next command.
 */
class Codefile
{
private:
    ///@brief the number of tabs that gives proper indentation
    int numTabs;
    ///@brief the name of the file this Codefile object represents
    string filename;
    ///@brief the string holding the contents of the file 
    string contents;
    
    /*!
     * @discussion adds the appropriate number of tabs to a new line
     */
    void addTabs();
    
    
    void checkInvariants();
    
public:
    
    /*!
    * @brief creates a new codefile with a filename
    * @param filename_in the filename this code object will write to, it should include the extension
    * @code Codefile file("filename.cpp")
    */
    Codefile(string filename_in);
    
    /// @brief writes the current Codefile to disk
    /// @return true if write is sucessful
    bool writeToFile();
    
    /*!
     * @discussion adds a single line to the codefile.
     * requires the string is syntactically correct.
     * requires this is an initalized Codefile.
     * @param line_in takes a single string in as a parameter.
     * @return returns true if the write is sucessful
     */
    bool writeLine(string line_in);
    
    /*!
     * @discussion starts an if block
     * @param expression the bolean expression needed for the condition of the if block
     * @return true if block was started sucessfully
     */
    bool startIf(string expression);
    
    
    /*!
     * @discussion ends an if block
     * @return returs true if block was ended sucessufully
     */
    bool endIf();
    
    /*!
     * @discussion starts an elseif block, alternate for an endIf()
     * @param expression boolean expression that the conditional will check
     * @return returns true if block was added sucessfully
     */
    bool startElseIf(string expression);
    
    /*!
     * @discussion ends a elseif block, identical to endIf() but should be used
     * with an ElseIf() block for style reasons
     * @return returns true if the block was ended sucessfully
     */
    bool endElseIf();
    
    /*!
     * @discussion starts an else block
     * @return returns true if the block was started sucessfully
     */
    
    bool startElse();
    
    /*!
     * @discussion ends an else block, equivelent to endIf() but should be used with startElse()
     * for style reasons.
     * @return returns true if the block is ended sucessfully.
     */
    
    bool endElse();
    
  
    /*!
     * @discussion starts a for loop
     * @param expression the three statements that make the for loops running conditions
     * @return true if it was sucessful
     */
    bool startFor(string expression);
    
    /*!
     * @discussion ends a for loop
     * @return returns true if block was ended sucessully
     */
    bool endFor();
    
    /*!
     * @discussion starts a while loop block
     * @param expression a boolean expression that controls the loop
     * @return true if the block is started sucessfully
     */
    
    bool startWhile(string expression);
    
    /*!
     * @discussion ends a while loop
     * @return returns true if block was added sucessfully
     */
    
    bool endWhile();
    
    /*!
     * @discussion starts a generic code block ie a properly indented curly brace.
     * @return returns true if the  block is sucessful
     */
    
    bool startBlock();
    
    /*!
     * @discussion ends a generic code block, ie a properly indented curly brace
     * @return returns true if its sucessul
     */
    bool endBlock();
};
#endif /* defined(__Autotester__codeWriter__) */
