//
//  TestCase.cpp
//  Autotester
//
//  Created by Joshua Trate on 5/7/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include "TestCase.hpp"
#include "codeWriter.hpp"
#include <iostream>
#include <cassert>

// checks that this file is still good
void TestCase::checkInvariants()
{
    assert(includes.size() >= 0);
    assert(setup.size() >= 0);
    assert(test.size() >= 0);
}

// adds an item to the include queue
void TestCase::addInclude(string include_in)
{
    checkInvariants();
    includes.push(include_in);
    checkInvariants();
}

// adds an item to the setup queue
void TestCase::addSetup(string setup_in)
{
    checkInvariants();
    setup.push(setup_in);
    checkInvariants();
}
// sets the test expression
void TestCase::setTest(string test_in)
{
    checkInvariants();
    test = test_in;
    checkInvariants();
}
// writes the test case to disk, using the given file name
void TestCase::writeTest(string filename)
{
    checkInvariants();
    Codefile testFile(filename);
    
   
    bool needNewLine = true;
    
    // note if this runs, the while loop will not
    // saves whether or not there were any includes.
    if (includes.size() == 0) needNewLine = false;

    // adds the includes to test case file
    while (includes.size() > 0)
    {
        testFile.writeLine("#include " + includes.front());
        includes.pop();
    }
    
    //adds a new line if coming after an include
    //adds using namespace std;
    if (needNewLine) testFile.writeLine("\n");
    testFile.writeLine("using namespace std;");
    
    // adds main and starts a block
    testFile.writeLine("\n");
    testFile.writeLine("int main()");
    testFile.startBlock();
    
    // adds setup statements one at a time

    while (setup.size() > 0)
    {
        testFile.writeLine(setup.front());
        setup.pop();
    }
    
    testFile.writeLine("assert(" + test + ");");
    testFile.writeLine("\n");
    testFile.writeLine("return 0;");
    testFile.endBlock();
    
    testFile.writeToFile();
    checkInvariants();
}