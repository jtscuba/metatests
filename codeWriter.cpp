//
//  codeWriter.cpp
//  Autotester
//
//  Created by Joshua Trate on 5/4/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include "codeWriter.hpp"

Codefile::Codefile(string filename_in): numTabs(0),filename(filename_in), contents("")
{
    checkInvariants();
}

bool Codefile::writeToFile()
{
    checkInvariants();
    ofstream outfile(filename.c_str());
    assert(outfile);
    assert(outfile << contents);
    checkInvariants();

    return true;
}


bool Codefile::writeLine(string line_in)
{
    checkInvariants();
    
    contents = contents + line_in + "\n";
    addTabs();
    checkInvariants();
    return true;
}


bool Codefile::startIf(string expression)
{
    checkInvariants();
    writeLine("if(" + expression +")");
    startBlock();
    checkInvariants();
    return true;
}


bool Codefile::endIf()
{
    checkInvariants();
    
    endBlock();
    
    checkInvariants();
    return true;
}


bool Codefile::startElseIf(string expression)
{
    checkInvariants();
    
    endBlock();
    writeLine("else if(" + expression + ")");
    startBlock();
    
    checkInvariants();
    
    return true;
}

bool Codefile::endElseIf()
{
    checkInvariants();
    
    endBlock();
    
    checkInvariants();
    return true;
}

bool Codefile::startElse()
{
    checkInvariants();
    
    endBlock();
    writeLine("else");
    startBlock();

    checkInvariants();
    return true;
}

bool Codefile::endElse()
{
    checkInvariants();
    
    endBlock();
    
    checkInvariants();
    return true;
}

bool Codefile::startFor(string expression)
{
    checkInvariants();
    
    writeLine("for(" + expression + ")");
    startBlock();
    
    checkInvariants();
    return true;
}

bool Codefile::endFor()
{
    checkInvariants();
    
    endBlock();
    
    checkInvariants();
    return true;
}

bool Codefile::startWhile(string expression)
{
    checkInvariants();
    writeLine("while(" + expression + ")");
    startBlock();
    checkInvariants();
    return true;
}

bool Codefile::endWhile()
{
    checkInvariants();
    
    endBlock();
    
    checkInvariants();
    return true;
}

//
bool Codefile::startBlock()
{
    checkInvariants();
    
    ++numTabs;
    contents += "{\n";
    addTabs();
    
    checkInvariants();
    return true;
}

// leaves a blank line after the last line in a block
// reduces the numTabs and adds a curly brace
// adds indentation for the next block of code
bool Codefile::endBlock()
{
    checkInvariants();
    
    --numTabs;
    contents += "\n";
    addTabs();
    contents += "}\n";
    addTabs();
    checkInvariants();
    return true;
}

// checks the member variables for problems
void Codefile::checkInvariants()
{
    assert(contents.size() >= 0);
    assert(filename.size() > 0);
    assert(numTabs >= 0);
}

// adds apropriate number of tabs before new code
void Codefile::addTabs()
{
    checkInvariants();
    for (int x = 0; x < numTabs; ++x)
    {
        contents += "\t";
    }
    checkInvariants();
}
