//
//  testCaseTest.cpp
//  Autotester
//
//  Created by Joshua Trate on 5/7/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include <iostream>
#include "TestCase.hpp"


using namespace std;

static void test1()
{
    TestCase t1;
    t1.writeTest("t1.cpp");
}

static void test2()
{
    TestCase t2;
    t2.addInclude("<iostream>");
    t2.addInclude("<fstream>");
    t2.writeTest("t2.cpp");
}

static void test3()
{
    TestCase t3;
    t3.addInclude("<iostream>");
    t3.addInclude("<fstream>");
    t3.addSetup("int x = 0;");
    t3.addSetup("int y = 42;");
    t3.addSetup("\n");
    t3.writeTest("t3.cpp");
}

static void test4()
{
    TestCase t4;
    t4.addInclude("<iostream>");
    t4.addInclude("<fstream>");
    t4.addSetup("int x = 0;");
    t4.addSetup("int y = 42;");
    t4.addSetup("\n");
    t4.setTest("y > x");
    t4.writeTest("t3.cpp");
}
int main()
{
    cout << "demo" << endl;
    
    // a simple test
    test1();
    
    // tests include features
    test2();
    
    // tests Setup features
    test3();
    
    // tests the assert features
    test4();
    return 0;
}