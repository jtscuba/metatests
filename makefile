CC=clang++
CFlags = -Wall -Werror -Wextra -pedantic -O3
CFlagsdebug = -Wall -Werror -Wextra -pedantic -g3
CodeWriterFiles = codeWriter.cpp CodeWriterTest.cpp
TestCaseFiles = codeWriter.cpp TestCase.cpp testCaseTest.cpp
ParserFiles = Type.cpp CodeParser.cpp ParserTest.cpp
TypeFiles = Type.cpp TypeTest.cpp

CodeWriterTest: $(CodeWriterFiles)
	$(CC) $(CFlags) $(CodeWriterFiles) -o CWriter

CodeWriterTestDebug: $(CodeWriterFiles)
	$(CC) $(CFlagsdebug) $(CodeWriterFiles) -o CWriter

TestCaseTest: $(TestCaseFiles)
	$(CC) $(CFlags) $(TestCaseFiles) -o TestCaseTest

TestCaseTestDebug: $(TestCaseFiles)
	$(CC) $(CFlagsdebug) $(TestCaseFiles) -o TestCaseTest

TypeTest: $(TypeFiles)
	$(CC) $(CFlags) $(TypeFiles) -o TypeTest

TypeTestDebug: $(TypeFiles)
	$(CC) $(CFlagsdebug) $(TypeFiles) -o TypeTest

CodeParser: $(ParserFiles)
	$(CC) $(CFlags) $(ParserFiles) -o parserTest
