//
//  TestCase.h
//  Autotester
//
//  Created by Joshua Trate on 5/7/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#ifndef __Autotester__TestCase__
#define __Autotester__TestCase__

#include <iostream>
#include "codeWriter.hpp"
#include <queue>

class TestCase
{
private:
    /*!
     *  A queue of libraries and files to include
     */
    queue<string> includes;
    
    /*!
     *  a queue of all the lines that are needed to setup the test case
     */
    queue<string> setup;
    
    /*!
     *  a boolean expression that evaluates to true if the program is working correctly
     *
     */
    string test;
    
    /*!
     *  checks whether all of the data is still valid.
     */
    void checkInvariants();
    
public:
    
    /*!
     *  adds an import to the test case
     *
     *  @param include_in a string holding an include statement
     *  @code
     *  Testcase T;
     *  T.addInclude("<fstream>");
     */
    void addInclude(string include_in);
    
    /*!
     *  Adds a line to the setup of the test case
     *
     *  @param setup_in a line that should be executed in order to set up the data needed for test to be evaluated
     *
     *  @code 
     *  TestCase T;
     *  T.addSetup("int testData = 6;")
     */
    void addSetup(string setup_in);
    
    /*!
     * sets the boolean expression that evaluates to true if the test passes
     *
     * @param test_in a string holding a valid boolean expression that is true
     * if the test passes
     * @code TestCase T;
     * T.setTest("x == getValue()")
     */
    void setTest(string test_in);
    
    /*!
     *  writes the test case currently held by this TestCase object to disk
     *
     *  @param filename The filename you want to write the test to
     *  @code 
     *  TestCase T;
     *  T.writeTest("TestCase1.cpp")
     */
    void writeTest(string filename);
    
    
    
    
    
};


#endif /* defined(__Autotester__TestCase__) */
