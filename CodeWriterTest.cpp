//
//  main.cpp
//  Autotester
//
//  Created by Joshua Trate on 4/30/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "codeWriter.hpp"

using namespace std;

// writeline tests
static void writeLineTests()
{
    Codefile test1("tests1.cpp");
    test1.writeLine("#include <cmath>");
    test1.writeLine("using namespace std;");
    test1.writeLine("int main()");
    test1.startBlock();

    test1.writeLine("return 0;");

    test1.endBlock();
    test1.writeToFile();
}



static void ifTests()
{
    Codefile test2("tests2.cpp");
    test2.writeLine("#include <cmath>");
    test2.writeLine("#include <iostream>");
    test2.writeLine("using namespace std;");
    test2.writeLine("int main()");
    test2.startBlock();
    test2.writeLine("int x = 0;");
    test2.writeLine("++x;");
    test2.startIf("x == 1");
    test2.writeLine("cout << \"hi\" << endl;");
    test2.endIf();
    test2.writeLine("return 0;");
    test2.endBlock();
    test2.writeToFile();
}

static void ifElseIfTest()
{
    Codefile test3("tests3.cpp");
    test3.writeLine("#include <cmath>");
    test3.writeLine("#include <iostream>");
    test3.writeLine("using namespace std;");
    test3.writeLine("int main()");
    test3.startBlock();
    test3.writeLine("int x = 0;");
    test3.writeLine("++x;");
    test3.startIf("x == 1");
    test3.writeLine("cout << \"hi\" << endl;");
    test3.startElseIf("x < 0");
    test3.writeLine("cout << \"go away\" << endl;");
    test3.endElseIf();
    test3.writeLine("return 0;");
    test3.endBlock();
    test3.writeToFile();
}

static void ifElseifElseTest()
{
    Codefile test4("tests4.cpp");
    test4.writeLine("#include <cmath>");
    test4.writeLine("#include <iostream>");
    test4.writeLine("using namespace std;");
    test4.writeLine("int main()");
    test4.startBlock();
    test4.writeLine("int x = 0;");
    test4.writeLine("++x;");
    test4.startIf("x == 1");
    test4.writeLine("cout << \"hi\" << endl;");
    test4.startElseIf("x < 0");
    test4.writeLine("cout << \"go away\" << endl;");
    test4.startElse();
    test4.writeLine("cout << \"error\" << endl;");
    test4.writeLine("return 0;");
    test4.endElse();
    test4.endBlock();
    test4.writeToFile();
}

static void forLoopTest()
{
    Codefile test5("tests5.cpp");
    test5.writeLine("#include <cmath>");
    test5.writeLine("#include <iostream>");
    test5.writeLine("using namespace std;");
    test5.writeLine("int main()");
    test5.startBlock();
    test5.writeLine("int x = 0;");
    test5.writeLine("++x;");
    test5.startIf("x == 1");
    test5.writeLine("cout << \"hi\" << endl;");
    test5.startFor("int x = 0; x < 20 ; ++x");
    test5.writeLine("cout << \"test: x is equal to \" << \"x\" << endl;");
    test5.endFor();
    test5.startElseIf("x < 0");
    test5.writeLine("cout << \"go away\" << endl;");
    test5.startElse();
    test5.writeLine("cout << \"error\" << endl;");
    test5.endElse();
    test5.writeLine("return 0;");
    test5.endBlock();
    test5.writeToFile();
}

static void nestedBlockTest()
{
    Codefile test7("tests7.cpp");
    test7.startBlock();
    test7.startBlock();
    test7.startBlock();
    test7.endBlock();
    test7.endBlock();
    test7.endBlock();
    test7.writeToFile();
}

static void whileLoopTest()
{
    Codefile test6("tests6.cpp");
    test6.writeLine("#include <cmath>");
    test6.writeLine("#include <iostream>");
    test6.writeLine("using namespace std;");
    test6.writeLine("int main()");
    test6.startBlock();
    test6.writeLine("int x = 0;");
    test6.writeLine("++x;");
    test6.startIf("x == 1");
    test6.writeLine("cout << \"hi\" << endl;");
    test6.startFor("int x = 0; x < 20 ; ++x");
    test6.writeLine("cout << \"test: x is equal to \" << \"x\" << endl;");
    test6.endFor();
    test6.startElseIf("x < 0");
    test6.writeLine("cout << \"go away\" << endl;");
    test6.writeLine("int x = 19;");
    test6.startWhile("x != 20");
    test6.writeLine("cout << x++ << endl;");
    test6.endWhile();
    test6.startElse();
    test6.writeLine("cout << \"error\" << endl;");
    test6.endElse();
    test6.writeLine("return 0;");
    test6.endBlock();
    test6.writeToFile();
}

/*!
 * @discussion this does nothing
 * @return returns zero
 */
int main(int argc, const char *argv[])
{
    // runs tests on the writeline test function
    writeLineTests();

    // runs tests on if statements
    ifTests();

    // runs tests on if-elseif blocks
    ifElseIfTest();

    // runs tests on if-elseif-else blocks
    ifElseifElseTest();

    // runs tests on for loop blocks
    forLoopTest();

    // runs tests on while loop blocks;
    whileLoopTest();

    // runs nested block test
    nestedBlockTest();

    cout << "done" << endl;
    return 0;
}
