//
//  TypeTest.cpp
//  Autotester
//
//  Created by Joshua Trate on 5/8/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include "Type.hpp"
#include <iostream>
#include <string>
#include <cassert>

using namespace std;

static void test1()
{
    string int_t = "int";
    assert(getType(int_t) == INT);
    assert(getType("string") == STRING);
    assert(getType("double") == DOUBLE);
    assert(getType("bool") == BOOLEAN);
    assert(getType("BOOL") == BOOLEAN);
    assert(getType("char") == CHAR);
    assert(getType("float") == FLOAT);
    assert(getType("void") == VOID);
    assert(getType("moneyBullShithere") == CUSTOM);
}

static void test2()
{
    assert(getTypeAsString(INT) == "int");
    assert(getTypeAsString(STRING) == "string");
    assert(getTypeAsString(DOUBLE) == "double");
    assert(getTypeAsString(BOOLEAN) == "bool");
    assert(getTypeAsString(CHAR) == "char");
    assert(getTypeAsString(FLOAT) == "float");
    assert(getTypeAsString(VOID) == "void");
    assert(getTypeAsString(CUSTOM) == "custom");
}

int main()
{
    // test1
    test1();

    // test2
    test2();

    cout << "type tests pass " << endl;
    return 0;
}
