//
//  CodeParser.h
//  Autotester
//
//  Created by Joshua Trate on 5/8/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#ifndef __Autotester__CodeParser__
#define __Autotester__CodeParser__

#include <iostream>
#include <vector>
#include "TestCase.hpp"
#include <string>
#include "Type.hpp"
using namespace std;

/*!
 *  a functionTests takes in a block of comments followed by a function implementaion and writes the test cases.
 *  It processes it and provides several output options.
 */
class FunctionTests
{

private:
    /*!
     *  This holds all the tests produced from parsing the code
     */
    vector<TestCase> tests;
    /*!
     *  this holds the raw test to be parsed for test cases.
     */
    string FunctionBlockWithComments;
    /*!
     *  this Holds the return type if its a primative type and custom if its a custom data type
     */
    type returnType;
    /*!
     *  holds all the parameters the function takes in if there primative types or custom if the type is not built in.
     */
    vector<type> parameters;

    // true if the block occures inside a class declaration or
    bool inClass;

public:
    // makes a new codeParser that can analyzes a given block
    FunctionTests(string block, bool inClass_in);

    // interprets the code found in a block and adds the values to testcase objects.
    void interpretCode();

    // writes all the tests to file
    // uses naming scheme defined in documentation
    void writeCode();
};

#endif /* defined(__Autotester__CodeParser__) */
