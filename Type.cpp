//
//  Type.cpp
//  Autotester
//
//  Created by Joshua Trate on 5/8/15.
//  Copyright (c) 2015 Joshua Trate. All rights reserved.
//

#include "Type.hpp"

type getType(string type_in)
{

    if (type_in == "int")
    {
        return INT;
    }
    else if (type_in == "string")
    {
        return STRING;
    }
    else if (type_in == "double")
    {
        return DOUBLE;
    }
    else if (type_in == "bool")
    {
        return BOOLEAN;
    }
    else if (type_in == "BOOL")
    {
        return BOOLEAN;
    }
    else if (type_in == "char")
    {
        return CHAR;
    }
    else if (type_in == "float")
    {
        return FLOAT;
    }
    else if (type_in == "void")
    {
        return VOID;
    }
    else
    {
        return CUSTOM;
    }
}

string getTypeAsString(type type_in)
{
    if (type_in == INT)
    {
        return "int";
    }
    else if (type_in == STRING)
    {
        return "string";
    }
    else if (type_in == DOUBLE)
    {
        return "double";
    }
    else if (type_in == BOOLEAN)
    {
        return "bool";
    }
    else if (type_in == CHAR)
    {
        return "char";
    }
    else if (type_in == FLOAT)
    {
        return "float";
    }
    else if (type_in == VOID)
    {
        return "void";
    }
    else
    {
        return "custom";
    }
}
